﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnTranslate = New System.Windows.Forms.Button()
        Me.btnAddNew = New System.Windows.Forms.Button()
        Me.tabCtrl = New System.Windows.Forms.TabControl()
        Me.tpTranslate = New System.Windows.Forms.TabPage()
        Me.tbTransLatin = New System.Windows.Forms.TextBox()
        Me.tbTransJapan = New System.Windows.Forms.TextBox()
        Me.tpAddNewWord = New System.Windows.Forms.TabPage()
        Me.tbAddLatin = New System.Windows.Forms.TextBox()
        Me.tbAddJapan = New System.Windows.Forms.TextBox()
        Me.btnChange = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.tabCtrl.SuspendLayout()
        Me.tpTranslate.SuspendLayout()
        Me.tpAddNewWord.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnTranslate
        '
        Me.btnTranslate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTranslate.Location = New System.Drawing.Point(6, 540)
        Me.btnTranslate.Name = "btnTranslate"
        Me.btnTranslate.Size = New System.Drawing.Size(871, 23)
        Me.btnTranslate.TabIndex = 0
        Me.btnTranslate.Text = "Translate"
        Me.btnTranslate.UseVisualStyleBackColor = True
        '
        'btnAddNew
        '
        Me.btnAddNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddNew.Location = New System.Drawing.Point(444, 540)
        Me.btnAddNew.Name = "btnAddNew"
        Me.btnAddNew.Size = New System.Drawing.Size(433, 23)
        Me.btnAddNew.TabIndex = 1
        Me.btnAddNew.Text = "Add new word"
        Me.btnAddNew.UseVisualStyleBackColor = True
        '
        'tabCtrl
        '
        Me.tabCtrl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabCtrl.Controls.Add(Me.tpTranslate)
        Me.tabCtrl.Controls.Add(Me.tpAddNewWord)
        Me.tabCtrl.Location = New System.Drawing.Point(12, 12)
        Me.tabCtrl.MaximumSize = New System.Drawing.Size(891, 595)
        Me.tabCtrl.MinimumSize = New System.Drawing.Size(891, 595)
        Me.tabCtrl.Name = "tabCtrl"
        Me.tabCtrl.SelectedIndex = 0
        Me.tabCtrl.Size = New System.Drawing.Size(891, 595)
        Me.tabCtrl.TabIndex = 2
        '
        'tpTranslate
        '
        Me.tpTranslate.BackColor = System.Drawing.Color.DimGray
        Me.tpTranslate.Controls.Add(Me.tbTransLatin)
        Me.tpTranslate.Controls.Add(Me.tbTransJapan)
        Me.tpTranslate.Controls.Add(Me.btnTranslate)
        Me.tpTranslate.Location = New System.Drawing.Point(4, 22)
        Me.tpTranslate.Name = "tpTranslate"
        Me.tpTranslate.Padding = New System.Windows.Forms.Padding(3)
        Me.tpTranslate.Size = New System.Drawing.Size(883, 569)
        Me.tpTranslate.TabIndex = 0
        Me.tpTranslate.Text = "Translate"
        '
        'tbTransLatin
        '
        Me.tbTransLatin.BackColor = System.Drawing.Color.Gray
        Me.tbTransLatin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbTransLatin.Cursor = System.Windows.Forms.Cursors.Default
        Me.tbTransLatin.Location = New System.Drawing.Point(454, 6)
        Me.tbTransLatin.Multiline = True
        Me.tbTransLatin.Name = "tbTransLatin"
        Me.tbTransLatin.Size = New System.Drawing.Size(423, 528)
        Me.tbTransLatin.TabIndex = 1
        Me.tbTransLatin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbTransJapan
        '
        Me.tbTransJapan.BackColor = System.Drawing.Color.Gray
        Me.tbTransJapan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbTransJapan.Cursor = System.Windows.Forms.Cursors.Default
        Me.tbTransJapan.Location = New System.Drawing.Point(6, 6)
        Me.tbTransJapan.Multiline = True
        Me.tbTransJapan.Name = "tbTransJapan"
        Me.tbTransJapan.Size = New System.Drawing.Size(423, 528)
        Me.tbTransJapan.TabIndex = 1
        Me.tbTransJapan.Text = "Input Japanese"
        Me.tbTransJapan.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tpAddNewWord
        '
        Me.tpAddNewWord.BackColor = System.Drawing.Color.DimGray
        Me.tpAddNewWord.Controls.Add(Me.DataGridView1)
        Me.tpAddNewWord.Controls.Add(Me.tbAddLatin)
        Me.tpAddNewWord.Controls.Add(Me.tbAddJapan)
        Me.tpAddNewWord.Controls.Add(Me.btnChange)
        Me.tpAddNewWord.Controls.Add(Me.btnAddNew)
        Me.tpAddNewWord.Location = New System.Drawing.Point(4, 22)
        Me.tpAddNewWord.Name = "tpAddNewWord"
        Me.tpAddNewWord.Padding = New System.Windows.Forms.Padding(3)
        Me.tpAddNewWord.Size = New System.Drawing.Size(883, 569)
        Me.tpAddNewWord.TabIndex = 1
        Me.tpAddNewWord.Text = "Add new word"
        '
        'tbAddLatin
        '
        Me.tbAddLatin.BackColor = System.Drawing.Color.Gray
        Me.tbAddLatin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbAddLatin.Location = New System.Drawing.Point(6, 72)
        Me.tbAddLatin.Multiline = True
        Me.tbAddLatin.Name = "tbAddLatin"
        Me.tbAddLatin.Size = New System.Drawing.Size(871, 60)
        Me.tbAddLatin.TabIndex = 3
        Me.tbAddLatin.Text = "Input Latin"
        Me.tbAddLatin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbAddJapan
        '
        Me.tbAddJapan.BackColor = System.Drawing.Color.Gray
        Me.tbAddJapan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbAddJapan.Location = New System.Drawing.Point(6, 6)
        Me.tbAddJapan.Multiline = True
        Me.tbAddJapan.Name = "tbAddJapan"
        Me.tbAddJapan.Size = New System.Drawing.Size(871, 60)
        Me.tbAddJapan.TabIndex = 2
        Me.tbAddJapan.Text = "Input Japanese"
        Me.tbAddJapan.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnChange
        '
        Me.btnChange.Location = New System.Drawing.Point(6, 540)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(433, 23)
        Me.btnChange.TabIndex = 1
        Me.btnChange.Text = "Change word"
        Me.btnChange.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(6, 138)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(871, 396)
        Me.DataGridView1.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(915, 618)
        Me.Controls.Add(Me.tabCtrl)
        Me.MaximumSize = New System.Drawing.Size(931, 657)
        Me.MinimumSize = New System.Drawing.Size(931, 657)
        Me.Name = "Form1"
        Me.Text = "Interpretator"
        Me.tabCtrl.ResumeLayout(False)
        Me.tpTranslate.ResumeLayout(False)
        Me.tpTranslate.PerformLayout()
        Me.tpAddNewWord.ResumeLayout(False)
        Me.tpAddNewWord.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnTranslate As Button
    Friend WithEvents btnAddNew As Button
    Friend WithEvents tabCtrl As TabControl
    Friend WithEvents tpTranslate As TabPage
    Friend WithEvents tpAddNewWord As TabPage
    Friend WithEvents tbAddLatin As TextBox
    Friend WithEvents tbAddJapan As TextBox
    Friend WithEvents tbTransLatin As TextBox
    Friend WithEvents tbTransJapan As TextBox
    Friend WithEvents btnChange As Button
    Friend WithEvents DataGridView1 As DataGridView
End Class
